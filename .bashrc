# To the extent possible under law, the author(s) have dedicated all 
# copyright and related and neighboring rights to this software to the 
# public domain worldwide. This software is distributed without any warranty. 
# You should have received a copy of the CC0 Public Domain Dedication along 
# with this software. 
# If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

# base-files version 4.1-1

# User dependent .bashrc file

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Aliases

alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
alias ls='ls -hF --color=tty'                 # classify files in colour
alias dir='ls --color=auto --format=vertical'
alias ll='ls -l'                              # long list
alias la='ls -A'                              # all but . and ..

alias apt-cyg="apt-cyg -m "http://mirrors.xmission.com/cygwin" -u"

# Use bash-completion, if available
if [ -f /etc/bash_completion ]; then
  . /etc/bash_completion
fi

npp () {
	/cygdrive/c/Program\ Files\ \(x86\)/Notepad++/notepad++.exe -multiInst -nosession -noPlugin $(cygpath -w -- "$@")
}


export PATH=$PATH:~/bin

if [ -f ~/environment_path ]; then
  . ~/environment_path
fi


set convert-meta off
set output-meta on
